# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('price', models.DecimalField(max_digits=20, decimal_places=2)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='lead',
            name='state',
        ),
        migrations.DeleteModel(
            name='LeadState',
        ),
        migrations.AddField(
            model_name='lead',
            name='products',
            field=models.ManyToManyField(to='core.Product'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='lead',
            name='name',
            field=models.CharField(max_length=255),
        ),
    ]
