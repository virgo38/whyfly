from django.core.exceptions import ValidationError
from django.test import TestCase
from core.models import LeadState, Lead


class MyTestCase(TestCase):

    def test_change_state(self):
        print self.test_change_state.__name__
        lead_state = LeadState.objects.create(name=LeadState.STATE_NEW)
        lead = Lead.objects.create(name='lead1', state=lead_state)

        lead.state_id = LeadState.STATE_IN_PROGRESS
        lead.name = 'new_name'
        lead.save()

        lead.state_id = LeadState.STATE_NEW
        self.assertRaises(ValidationError, lead.save)