# -*-coding:utf-8-*-
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver


class LeadState(models.Model):
    # pk инстансов модели
    STATE_NEW = 1  # Новый
    STATE_IN_PROGRESS = 2  # В работе
    STATE_POSTPONED = 3  # Приостановлен
    STATE_DONE = 4  # Завершен

    name = models.CharField(u"Название", max_length=50, unique=True, )


class Lead(models.Model):
    ORDERS = (
        (LeadState.STATE_NEW, LeadState.STATE_IN_PROGRESS),
        (LeadState.STATE_IN_PROGRESS, LeadState.STATE_POSTPONED),
        (LeadState.STATE_IN_PROGRESS, LeadState.STATE_DONE),
        (LeadState.STATE_POSTPONED, LeadState.STATE_DONE),
    )

    name = models.CharField(
        max_length=255,
        db_index=True,
        verbose_name=u"Имя",
    )
    state = models.ForeignKey(
        LeadState,
        on_delete=models.PROTECT,
        default=LeadState.STATE_NEW,
        verbose_name=u"Состояние",
    )

    def save(self, force_insert=False, force_update=False, using=None,
         update_fields=None):
        # Проверяем последовательность инстансов
        if self.pk is None:
            if self.state_id != LeadState.STATE_NEW:
                raise ValidationError({'state_id': ['The wrond initial state.']})
        else:
            old_state_id = Lead.objects.get(pk=self.pk).state_id
            try:
                self.ORDERS.index((old_state_id, self.state_id))
            except ValueError:
                raise ValidationError({'state_id': ['The wrond order.']})

        super(Lead, self).save(force_insert, force_update, using,
                                    update_fields)


class Action(object):
    """
    Класс содержит методы с дополнительной бизнес-логикой для каждого
    переходного состояния объекта LeadState.
    Использование:
    act = Action()
    act(state_id)
    """
    def __init__(self):
        super(Action, self).__init__()

    @staticmethod
    def state_in_progress():
        print Action.state_in_progress.__name__

    @staticmethod
    def state_postponed():
        print Action.state_postponed.__name__

    @staticmethod
    def state_done():
        print Action.state_done.__name__

    def __call__(self, state_id):
        if state_id == LeadState.STATE_IN_PROGRESS:
            self.state_in_progress()
        elif state_id == LeadState.STATE_POSTPONED:
            self.state_postponed()
        elif state_id == LeadState.STATE_DONE:
            self.state_done()


@receiver(post_save, sender=Lead)
def procesing_method(instance, **kwargs):
    """
    Получаем сигнал об изменении сущности, и выполняем метод с дополнительной
    бизнес логикой
    :param instance:
    :param kwargs:
    :return:
    """
    if instance.pk is None:
        return
    old = Lead.objects.get(pk=instance.pk)
    if old.state_id != instance.state_id:
        action = Action()
        action(instance.state_id)