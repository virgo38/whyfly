from django.views.generic import TemplateView
from core2.models import Lead, Product


class Home(TemplateView):
    template_name = 'core2/home.html'

    def get_context_data(self, **kwargs):
        data = super(Home, self).get_context_data(**kwargs)
        data['list'] = Lead.get_all_leads_with_short_info()
        return data

    def recreate_entries(self):
        """
        Пересоздает Lead и Product
        :return:
        """
        COUNT = 6
        Product.objects.all().delete()
        Lead.objects.all().delete()

        for i in range(1, COUNT):
            lead = Lead.objects.create(name='lead%s' % i)
            for j in range(1, COUNT):
                prod = Product.objects.create(title='p%s' % j,
                                              price='%s' % j * 10)
                lead.products.add(prod)
            lead.save()