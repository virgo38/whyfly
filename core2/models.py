# -*-coding:utf-8-*-
from django.db import models


class Product(models.Model):
    title = models.CharField(max_length=255)
    price = models.DecimalField(max_digits=20, decimal_places=2)

    def __unicode__(self):
        return self.title


class Lead(models.Model):
    name = models.CharField(max_length=255)
    products = models.ManyToManyField(Product)

    def get_all_lead_info(self):
        products = ["{0}".format(product.title) for product in
                    self.products.all()]
        return u"name: {name}, products = {products}".format(
            name=self.name, products=", ".join(products))

    @classmethod
    def get_all_leads_with_short_info(cls):
        return [lead.get_all_lead_info()
                for lead in Lead.objects.prefetch_related('products')]

