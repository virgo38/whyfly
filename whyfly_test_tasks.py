#-*-coding:utf-8-*-
import unittest

__author__ = 'DEsipov[for29you@gmail.com]'

###############################################################################
# Задание 1.
###############################################################################
# Необходимо реализовать сущность accumulator, которая бы соответствовала
# следующему интерфейсу взаимодействия:
#  acс = accumulator(10)
#  assert acc(2) == 12
#  assert acc(3) == 15
#  assert acc(100) == 115
#  Можно ли это сделать через класс (если да, то как)? Можно ли это сделать
# через функцию (если да, то как)?


def accumulator(a):
    """
    Реализация через аттрибут функции

    :param a:
    :return:
    """
    accumulator.__sum = a

    def f(b):
        accumulator.__sum = accumulator.__sum + b
        return accumulator.__sum
    return f


class Accumulator:
    """
    Реализация через класс-функтор
    """
    def __init__(self, a):
        self.sum = a

    def __call__(self, b):
        self.sum = self.sum + b
        return self.sum


###############################################################################
#Задание 2.
###############################################################################
# В Django проекте используется внешний сервис (какой именно - не важно). Для
# простоты предположим, что всё взаимодействие с данным сервисом заключается
# в отправка одного POST запроса. Взаимодействие с сервисом реализовано через
# класс FooBarService, в котором вызывается 1 метод:
# service = FooBarService()
# service.doRequest()
# Таких участков кода в проекте очень много. Каким образом можно сделать так,
# что при DEBUG == True реальный POST запрос не отправлялся,
# а при DEBUG == False - отправлялся? Замечание: использовать конструкции вида
# if settings.DEBUG:
# # не делать POST запрос
# else:
# # делать POST запрос
# не желательны.


DEBUG = True    # Аналог settings.DEBUG.


def debug_decorator(f):
    """
    Декоратор. Если DEBUG == True - выполняем декорируемую функцию, если
    False - то нет.

    :param f: декорируемая функция или метод
    :return:
    """
    def foo(*args, **kwargs):
        if DEBUG:
            print '(DEBUG=%s: ) %s: No request sent...' % (DEBUG, f.__name__)
            return False

        return f(*args, **kwargs)
    return foo


class FooBarService(object):

    @debug_decorator
    def doRequest(self):
        print '(DEBUG=%s: ) %s: Request sent...' % (
            DEBUG, self.doRequest.__name__)
        return True


class MyTestCase(unittest.TestCase):
    """
    Тесты для первых двух заданий.
    """
    def test_accumulator_func(self):
        print self.test_accumulator_func.__name__
        acc = accumulator(10)
        self.assertTrue(acc(2) == 12)
        self.assertTrue(acc(3) == 15)
        self.assertTrue(acc(100) == 115)

    def test_accumulator_class(self):
        print self.test_accumulator_class.__name__
        acc = Accumulator(10)
        self.assertTrue(acc(2) == 12)
        self.assertTrue(acc(3) == 15)
        self.assertTrue(acc(100) == 115)

    def test_foo_bar_service(self):
        print self.test_accumulator_class.__name__
        global DEBUG
        service = FooBarService()
        DEBUG = False
        self.assertTrue(service.doRequest())
        DEBUG = True
        self.assertFalse(service.doRequest())


if __name__ == '__main__':
    unittest.main()


###############################################################################
#Задание 3.
###############################################################################
# Допустим, что есть Django модель Lead и набор состояний, в котором может
# находится инстанс этой модели. Набор состояний определяется связью с другой
# моделью LeadState. Набор инстансов модели LeadState строго определен:
# class LeadState(models.Mode):
#     # pk инстансов модели
#     STATE_NEW = 1 # Новый
#     STATE_IN_PROGRESS = 2 # В работе
#     STATE_POSTPONED = 3 # Приостановлен
#     STATE_DONE = 4 # Завершен
#     name = models.CharField(u"Название",
#     max_length=50,
#     unique=True,
# )
# class Lead(models.Model):
#     name = models.CharField(
#     max_length=255,
#     db_index=True,
#     verbose_name=u"Имя",
#     )
#     state = models.ForeignKey(
#     LeadState,
#     on_delete=models.PROTECT,
#     default=LeadState.STATE_NEW,
#     verbose_name=u"Состояние",
#     )
# Возможность перехода из одного состояния в другое строго определена (с учетом
# направления перехода):
# Новый -> В работе
# В работе -> Приостановлен
# В работе -> Завершен
# Приостановлен -> В работе
# Приостановлен -> Завершен
# Другие переходы невозможны. В момент перехода из одного состояния в другое
# нужно вызвать метод, который реализует дополнительную бизнес логику
# (свой метод для каждого из типов переходов). Каким образом вы бы реализовали
# эту задачу? Решение необходимо в рамках Django Framework.

###############################################################################
#Ответ:
# Чтобы реализовать порядок переходов добавил аттрибут Lead.ORDERS и
# и переопределил метод Lead.save(...), вставив проверку, при сохранении.
#
# class Lead(models.Model):
#     # Строго определенный порядок переходов.
#     ORDERS = (
#         (LeadState.STATE_NEW, LeadState.STATE_IN_PROGRESS),
#         (LeadState.STATE_IN_PROGRESS, LeadState.STATE_POSTPONED),
#         (LeadState.STATE_IN_PROGRESS, LeadState.STATE_DONE),
#         (LeadState.STATE_POSTPONED, LeadState.STATE_DONE),
#     )
#
#     name = models.CharField(
#         max_length=255,
#         db_index=True,
#         verbose_name=u"Имя",
#     )
#     state = models.ForeignKey(
#         LeadState,
#         on_delete=models.PROTECT,
#         default=LeadState.STATE_NEW,
#         verbose_name=u"Состояние",
#     )
#
#     def save(self, force_insert=False, force_update=False, using=None,
#          update_fields=None):
#         # Проверяем последовательность инстансов
#         if self.pk is None:
#             if self.state_id != LeadState.STATE_NEW:
#                 raise ValidationError({'state_id': ['The wrond initial state.']})
#         else:
#             old_state_id = Lead.objects.get(pk=self.pk).state_id
#             try:
#                 self.ORDERS.index((old_state_id, self.state_id))
#             except ValueError:
#                 raise ValidationError({'state_id': ['The wrond order.']})
#
#         super(Lead, self).save(force_insert, force_update, using,
#                                     update_fields)
#
# Для реализации дополнительной бизнес-логики, при переходах использовал
# встроенные сигналы моделей django
# class Action:
#     """
#     Класс содержит методы с дополнительной бизнес-логикой для каждого
#     переходного состояния объекта LeadState.
#     Использование:
#     act = Action()
#     act(state_id)
#     """
#      @staticmethod
#     def state_in_progress():
#         print Action.state_in_progress.__name__
#
#     @staticmethod
#     def state_postponed():
#         print Action.state_postponed.__name__
#
#     @staticmethod
#     def state_done():
#         print Action.state_done.__name__
#
#     def __call__(self, state_id):
#         if state_id == LeadState.STATE_IN_PROGRESS:
#             self.state_in_progress()
#         if state_id == LeadState.STATE_POSTPONED:
#             self.state_postponed()
#         if state_id == LeadState.STATE_DONE:
#             self.state_done()
#
#
# @receiver(post_save, sender=Lead)
# def procesing_method(instance, **kwargs):
#     """
#     Получаем сигнал об изменении сущности, и выполняем метод с дополнительной
#     бизнес логикой
#     :param instance:
#     :param kwargs:
#     :return:
#     """
#     if instance.pk is None:
#         return
#     old = Lead.objects.get(pk=instance.pk)
#     if old.state_id != instance.state_id:
#         action = Action()
#         action(instance.state_id)


###############################################################################
#Задание 4.
###############################################################################
# В рамках Django проекта есть метод (не важно какого класса) покупки
# уникального товара (модель Product) c неким уникальным
# идентификатором item_id:
# @classmethod
# @transaction.atomic
# def buy(cls, user, item_id):
#     product_qs = Product.objects.filter(item_id=item_id)
#     if product_qs.exists():
#         product = product_qs[0]
#     if product.available:
#         # списание средств со счета пользователя
#         user.withdraw(product.price)
#         # информация о купленом товаре
#         send_email_to_user_of_buy_product(user)
#         product.available = False
#         product.buyer = user
#         product.save()
#         return True
#     else:
#         return False
# Есть ли в реализации данного метода потенциальные проблемы(проблема)?
# Интерфейс метода не брать во внимание.

###############################################################################
#Ответ
# 1. Данные может изменить другой процесс. Чтобы блокировать данные во
# время изменения необходимо добавить метод select_for_update
# Product.objects.select_for_update.filter(item_id=item_id)
# будем считать, что в user.withdraw тоже используется select_for_update
#
# 2. Транзакция влияет на производительность сервера БД. Поэтому
# рекомендуется делать ее как можно короче.
# Если письма отправляются, например, не через очередь, а через сторонний
# сервер и в том же потоке, то django будет ждать, пока сервер не пришлет
# ответ. В это случае лучше вынести отправку из транзакции,
# использую контекст-менеджер:
#     with transaction.atomic():
#         ...
#         product.save()
#
#     send_email_to_user_of_buy_product(user)
#
# 3. У пользователя может не хватить денег на покупку товара. В этом случае
# user.withdraw(product.price) должен возбуждать исключение, например,
# WithdrawException. Возможно, это уже реализовано, но тоже решил
# обратить внимание.
#
# 4. Так как идетификатор уникальный, то вместо filter, я бы использовал
# get и обработку исключения для улучшения читабельности (но это дело вкуса).
#
# Итого:
# @classmethod
# def buy(cls, user, item_id):
#     try:
#         with transaction.atomic():
#             # Используем select_for_update, для гарантии того, что другой
#             # поток не изменит данные.
#             product = Product.objects.select_for_update.get(pk=item_id)
#             if not product.available:
#                 return False
#
#             # списание средств со счета пользователя
#             user.withdraw(product.price)
#             # информация о купленом товаре
#             product.available = False
#             product.buyer = user
#             product.save()
#     # Возможное исключение, если у пользователя не хватило денег. Или нет
#     # product с item_id
#     except (WithdrawException, Product.DoesNotExist), ex:
#         return False
#
#     # Выносим отправку почты из транзакции.
#     send_email_to_user_of_buy_product(user)
#     return True


###############################################################################
# Задание 5
# В рамках Django проекта, даны модели:
# class Product(models.Model):
#     title = models.CharField(max_length=255)
#     price = models.DecimalField(max_digits=20, decimal_places=2)
#
# class Lead(models.Model):
#     name = models.CharField(max_length=255)
#     products = models.ManyToManyField(Product)
#
#     def get_all_lead_info(self):
#         products = ["{0} - {1}".format(product.title, product.price) for product in
#         self.products.all()]
#         return u"{name}: {products}".format(name=self.name,
#                                             products=", ".join(products))
#     @classmethod
#     def get_all_leads_with_short_info(cls):
#         return [lead.get_all_lead_info() for lead in Lead.objects.all()]
#
# # Что бы вы сказали о производительности метода get_all_leads_with_short_info с точки
# # зрения базы данных? Можно ли улучшить ситуацию?


###############################################################################
# Ответ:
# Запросы к БД неоптимальны. При высокой нагрузке это будет критично.
# Количество запросов будет равно количеству сущностей Lead в БД + 1.
# Lead.objects.all() - один запрос
# self.products.all() - по запросу на каждый объект Lead

# Если добавить prefetch_related. То кол-во запросов всегда будет равно 2.
#     @classmethod
#     def get_all_leads_with_short_info(cls):
#         return [lead.get_all_lead_info()
#                     for lead in Lead.objects.prefetch_related('products')]